//
//  MainViewController.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit

final class MainViewController: BaseViewController {
    
    
    @IBOutlet weak var containerView: MainView!
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - navigation
    @objc override func isShowingNavigationBar() -> Bool {
        return false
    }
}
