//
//  MainInteractor.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation


protocol MainInteractorProtocol: class {
    func loadData()
}


enum MainInteractorOutput {
    case didLoad
}


protocol MainInteractorOutputHandler: class {
    
}

final class MainInteractor {
    
}
