//
//  MainBuilder.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit

final class MainBuilder {
    static func make() -> MainViewController {
        
//        let interactor =
//        let presenter =
        let vc = MainViewController.Screen.main.instance(type: MainViewController.self)
        
        return vc
    }
}
