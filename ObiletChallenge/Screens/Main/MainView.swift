//
//  MainView.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


final class HeaderView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
}


final class TicketSegmentView: UIView {
    
    @IBOutlet weak var busTicketButton: UIButton!
    @IBOutlet weak var flightTicketButton: UIButton!
    
    
    lazy var selectedView: UIView =  {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }()
    
    var lastSelectedButton: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 3.0
        
        clipsToBounds = true
        layer.cornerRadius = frame.size.height / 2.0
        layer.masksToBounds = true
        
        
        self.addSubview(selectedView)
        self.sendSubviewToBack(selectedView)
        
        [busTicketButton, flightTicketButton].forEach {
            $0?.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            $0?.setTitleColor(.white, for: .selected)
            $0?.setTitleColor(.red, for: .normal)
            $0?.setBackgroundImage(nil, for: .selected)
            $0?.tintColor = .clear
        }
        
        buttonTapped(busTicketButton)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectedView.layer.cornerRadius = selectedView.frame.size.height / 2.0
        
        if let selected = lastSelectedButton {
            
            var frame = selectedView.frame
            frame.size.height = self.frame.size.height
            frame.size.width = self.frame.size.width / 2.0
            self.selectedView.frame = frame
            self.selectedView.center = selected.center
        }
    }
    
    @objc func buttonTapped(_ button: UIButton) {
        
        lastSelectedButton?.isSelected = false
        selectedView.center = button.center
        button.isSelected = true
        lastSelectedButton = button
    }
    
    
    
}

final class MainView: UIView {
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var segmentView: TicketSegmentView!
    
    
}



final class DirectionContainerView: ContainerView {
    
    @IBOutlet weak var fromLocationView: EditableView!
    @IBOutlet weak var toLocationView: EditableView!
    @IBOutlet weak var toggleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
}


final class EditableView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
}


public class ContainerView: UIView {
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 3.0
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.layer.borderWidth = 0.3
    }
}
