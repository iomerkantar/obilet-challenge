//
//  EntranceInteractor.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation

protocol EntranceInteractorProtocol {
    func loadSession()
}

enum EntranceInteractorOutput {
    case getSession(DeviceSessionModel)
}

protocol EntranceInteractorOutputHandler: class {
    func handler(_ output: EntranceInteractorOutput) -> Void
}

final class EntranceInteractor: EntranceInteractorProtocol {
   
    
    var provider = SessionProvider()
    
    weak var presenter: EntranceInteractorOutputHandler?
   
    func loadSession() {
        
        provider.request(type: DeviceSessionModel.self, target: .getSession) { [weak self] (response) in
            app.network.storage.deviceSession = response
            self?.presenter?.handler(.getSession(response))

        }
    }
    
}
