//
//  EntrancePresenter.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


protocol EntrancePresenterProtocol: class {
    func loadSession()
}

enum EntrancePresenterOutput {
    case failure(message: String)
    case success
}

protocol EntrancePresenterOutputHandler: class {
    func handler(_ output: EntrancePresenterOutput)
}

final class EntrancePresenter: EntrancePresenterProtocol, EntranceInteractorOutputHandler {
    
    
    var interactor: EntranceInteractorProtocol?
    
    weak var view: EntrancePresenterOutputHandler?
    
    
    func loadSession() {
        interactor?.loadSession()
    }
    
    func handler(_ output: EntranceInteractorOutput) {
        switch output {
        case .getSession(let _):
            view?.handler(.success)
            
        }
    }
    
}


