//
//  EntranceView.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit

final class EntranceView: UIView {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimating()
    }
    
}
