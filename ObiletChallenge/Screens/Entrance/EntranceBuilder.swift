//
//  EntranceBuilder.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation
import UIKit


final class EntranceBuilder {
    static func make() -> EntranceViewController {
        
        let interactor = EntranceInteractor()
        let presenter = EntrancePresenter()
        let screen = EntranceViewController.Screen.entrance.instance(type: EntranceViewController.self)
        
        interactor.presenter = presenter
        presenter.interactor = interactor
        screen.presenter = presenter
        presenter.view = screen
        
        return screen
    }
}
