//
//  EntranceViewController.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


final class EntranceViewController: BaseViewController, EntrancePresenterOutputHandler {
    
    
    @IBOutlet weak var containerView: EntranceView!
    
    
    var presenter: EntrancePresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.activityIndicatorView.startAnimating()
        presenter?.loadSession()
        
    }
    
    
    
    
    
    // MARK: - EntrancePresenterOutputHandler
    func handler(_ output: EntrancePresenterOutput) {
//        switch output {
//        case .success:
//            break
//
//        }
        
        DispatchQueue.main.async {
            
            
            self.view.window?.rootViewController = TabBarBuilder.make()
        }
    }
}
