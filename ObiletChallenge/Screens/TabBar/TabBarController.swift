//
//  TabBarViewController.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


fileprivate enum TabBarChild {
    case main
    case tickets
    case support
    case account
    
    var screen: UIViewController {
        switch self {
        case .main:
            return MainBuilder.make()
        case .tickets:
            return TicketsBuilder.make()
        default:
            return UIViewController()
        }
    }
}

final class TabBarController: UITabBarController {
    
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        build()
    }
    
    
    // MARK: - build
    func build() {
        let visibleChilds: [TabBarChild] = [.main, .tickets, .support, .account]
        
        var viewControllers: [UIViewController] = []
        
        for child in visibleChilds {
            let vc = child.screen
            vc.tabBarItem.title = "\(child)"
            viewControllers.append(UINavigationController(rootViewController: vc))
        }
        
        setViewControllers(viewControllers, animated: false)
        
    }
    
    
    
}
