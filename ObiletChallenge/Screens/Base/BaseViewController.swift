//
//  BaseViewController.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit



public class BaseViewController: UIViewController, ProviderDelegate {
    
    // bu class navigation bar life cycle icin
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        designingNavigationBar()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        designingNavigationBar()

    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        designingNavigationBar()

    }
    
    
    // MARK: - Network
    public func didLoadResult(_ result: APIManager.Result, withTarget: APITarget) {
        
    }
    
    public func resultError(_ error: Error, withTarget: APITarget) {
        
    }

}

// MARK: - NavigationBar Design
extension BaseViewController {
    // true ise background color .clear olacak
    @objc func isShowingNavigationBar() -> Bool {
        return false
    }
    
    @objc func getNavigationBarTitle() -> String? {
        return nil
    }
    
    @objc func navigationBarBackgroundColor() -> UIColor? {
        return .clear
    }
    
    func designingNavigationBar() {
        let backgroundColor: UIColor = isShowingNavigationBar() ? navigationBarBackgroundColor() ?? .clear : .clear
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backIndicatorImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = UIColor.red
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    
}

