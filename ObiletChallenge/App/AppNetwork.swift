//
//  AppNetwork.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation



// MARK: - bu class application ag yonetimini gerceklestirmektedir. bu ag api de olabilir, bluetooth, location vs gibi iletisim katmani olabilir.

final class AppNetwork {
    
    // TODO: - farkli farkli ortamlar olusturulabilir
    
    public enum Environment {
        case prod
        case test
        case mock
        
        var apiConfig: APIManager.Config {
            return APIManager.Config(baseURL: URL.baseURL(withEnvironment: self), headers: ["Content-Type": "application/json",
                                                                                            "Authorization": "Basic R21STG5Qd0E5QmwwS0w1UlN6L3VsZz09"])
        }
    }
    
    var api: APIManager
    
    var storage: Storage = Storage()
    
    init(_ environment: Environment = .prod) {
        api = APIManager(config: environment.apiConfig)
        
        // TODO: - dilersem headers guncellenebilir
//        api.set(headers: [<#T##[String : String]#>])
    }
    
    
}




// MARK: - url
fileprivate extension URL {
    
    static func baseURL(withEnvironment env: AppNetwork.Environment) -> URL {
        return URL(string: "https://v2-api.obilet.com/api/")!
    }
}
