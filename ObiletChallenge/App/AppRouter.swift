//
//  AppRouter.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


final class AppRouter {
    
    var window: UIWindow
    
    init() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        window.layer.cornerRadius = 4.0
        window.layer.masksToBounds = true
        window.makeKeyAndVisible()
    }
    
    
    func start() {
        window.rootViewController = EntranceBuilder.make()
    }
    
    
}
