//
//  AppContainer.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit


let app = AppContainer()

final class AppContainer {
    
    var router: AppRouter
    
    var network: AppNetwork
    
    
    init() {
        
        router = AppRouter()
        
        network = AppNetwork()
    }
}
