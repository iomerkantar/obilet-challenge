//
//  DeviceSessionModel.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit
import AdSupport

public struct DeviceSessionModel: Codable {
    
    var id: String = ""
    var deviceId: String = ""


    enum CodingKeys: String, CodingKey {
        
        case id = "session-id"
        case deviceId = "device-id"
        
    }
}
