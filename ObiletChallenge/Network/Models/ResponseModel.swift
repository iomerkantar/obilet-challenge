//
//  ResponseModel.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation

struct ResponseModel: Codable {
    
    var status: String = ""
    var message: String?
    var userMessage: String?
    var controller: String?
    var data: Data?
    
    func hasError() -> Bool {
        return status.lowercased() != "success"
    }
    
    enum CodingKeys: String, CodingKey {
        
        case status
        case message
        case userMessage = "user-message"
        case controller
        case data

    }
}
