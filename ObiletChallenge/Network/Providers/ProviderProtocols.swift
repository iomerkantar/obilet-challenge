//
//  ProviderProtocols.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation


public protocol ProviderDelegate: class {
    func didLoadResult(_ result: APIManager.Result, withTarget: APITarget)
    func resultError(_ error: Error, withTarget: APITarget)
}

public protocol ProviderProtocol: class {
    
    associatedtype ProviderTarget: APITarget
    
    // weak referance olacak
    var delegate: ProviderDelegate? { set get }
}

extension ProviderProtocol {

    
    public func request<T: Codable>(type: T.Type, target: ProviderTarget, success: @escaping (T) -> Void) {
        
        let api = app.network.api
        
        api.request(target: target) { [weak self] (result) in

            self?.delegate?.didLoadResult(result, withTarget: target)

            switch result {
            case .success(let response):
                
                let decoder = JSONDecoder()

                guard let data = response["data"] as? [String: Any], let modelData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted), let object = try? decoder.decode(T.self, from: modelData) else {
                    return
                }
                
                success(object)
                
            case .failure(let error):
                self?.delegate?.resultError(error, withTarget: target)
            }
        }
    }
}
