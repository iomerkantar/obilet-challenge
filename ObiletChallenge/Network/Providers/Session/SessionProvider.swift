//
//  SessionProvider.swift
//  ObiletChallenge
//
//  Created by omer kantar on 18.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit
import AdSupport


final class SessionProvider: ProviderProtocol {
    
    typealias ProviderTarget = SessionTarget
    
    public enum SessionTarget: APITarget {
        
        case getSession
        
        
        public var urlPath: String {
            switch self {
            case .getSession:
                return "client/getsession"
            }
        }
        
        public var method: APIManager.Method {
            return .post
        }
        
        public var parameters: [String : Any]? {
            switch self {
            case .getSession:
                
                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String

                return ["type": 2, "connection": [:], "application": ["version": appVersion, "equipment-id": UIDevice.current.identifierForVendor?.uuidString ?? ""]]
            }
        }
        
        public var headers: [String : String]? {
            return nil
        }

    }
    
    weak var delegate: ProviderDelegate?
    
}
