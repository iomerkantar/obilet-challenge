//
//  NetworkManager.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import Foundation


typealias APICompletion = (APIManager.Result) -> Void

public protocol APITarget {
    var urlPath: String { get }
    var method: APIManager.Method { get }
    var parameters: [String: Any]? { get }
    var headers: [String: String]? { get }
}

// MARK: - Request
public struct APIManager {
    
    // MARK: - Config
    struct Config {
        var baseURL: URL
        var headers: [String: String]
    }
    
    
    // MARK: - initialize
    
    var config: Config
    
    
    init(config: Config) {
        self.config = config
        self.build(withConfig: config)
    }
    
    // MARK: - build
    private func build(withConfig config: Config) {
        
    }
    
    // Yeap mutating like evulation theory 🧚🏾‍♀️
    mutating public func set(headers: [String: String]) {
        self.config.headers = headers
    }
    
    // MARK: - Result
    public enum Result {
        case success([String: Any])
        case failure(Error)
    }
    
    
    // MARK: - Request
    
    func request(target: APITarget, completion: APICompletion? = nil) {
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        let urlString = self.config.baseURL.absoluteString + target.urlPath
        
        guard let url = URL(string: urlString) else {
            print("ERROR API Manager \(#function) target: \(target)")
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = target.method.httpMethod
        
        
        if let parameters = target.parameters,
            let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])  {
            urlRequest.httpBody = httpBody
        }

        var headers: [String: String] = self.config.headers
        
        if let targetHeaders = target.headers {
            for (key, value) in targetHeaders {
                headers[key] = value
            }
        }
        
        // headers
        urlRequest.allHTTPHeaderFields = headers
        
        let task = session.dataTask(with: urlRequest) { data, response, error in
            
            
            if let err = error  {
                print ("API ERROR1: \(error!)")
                completion?(.failure(err))
                return
            }
            
            guard let content = data else {
                print("No data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.allowFragments)) as? [String: Any] else {
                print("JSON formatinda degildir")
                return
            }
            
            completion?(.success(json))
            print("RESULT \n \(json)")

        }
        
        task.resume()

    }
}


// MARK: - Methods
extension APIManager {
    public enum Method {
        case get
        case post
        case update
        case put
        case patch
        case delete
        
        fileprivate var httpMethod: String {
            return "\(self)".uppercased()
        }
    }
}
