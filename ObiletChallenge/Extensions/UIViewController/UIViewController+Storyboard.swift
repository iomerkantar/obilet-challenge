//
//  UIViewController+Storyboard.swift
//  ObiletChallenge
//
//  Created by omer kantar on 17.06.2019.
//  Copyright © 2019 queercodersistanbul. All rights reserved.
//

import UIKit

extension UIViewController {
    fileprivate enum Storyboard: String {
        case main = "Main"
        case tickets = "Tickets"
        case entrance = "Entrance"
        
        var instance: UIStoryboard {
            return UIStoryboard(name: rawValue, bundle: nil) // Bundle.main default bundle
        }
    }
    
    public enum Screen: String {
        case entrance = "EntranceVC"
        case main = "MainVC"
        case tickets = "TicketsVC"

        fileprivate var storyboardType: Storyboard {
            switch self {
            case .main:
                return .main
            case .tickets:
                return .tickets
            case .entrance:
                return .entrance
            }
        }
        
        
        public func instance<T: UIViewController>(type: T.Type) -> T {
            return storyboardType.instance.instantiateViewController(withIdentifier: rawValue) as! T
        }
    }
    
    
    public func push(withScreen screen: Screen) {
      
        let vc = screen.instance(type: UIViewController.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
